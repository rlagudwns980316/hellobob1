from flask import Flask
from flask import request

app = Flask(__name__)

class Calc:
    @app.route('/')
    def index():
        return 'Hello bob From KHJ'

    @app.route('/add')
    def add():
        a = int(request.args.get("a"))
        b = int(request.args.get("b"))

        return str(a+b)

    @app.route('/sub')
    def sub():
        a = int(request.args.get("a"))
        b = int(request.args.get("b"))
        return str(a - b)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8051,debug=True)


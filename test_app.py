import unittest
import app
import requests

class UnitTest(unittest.TestCase):
    def setUp(self):
        self.url = "http://13.209.15.210:8051"
        self.data={'a':'5', 'b':'2'}
        self.client = app.app.test_client()
        
    def test_add(self):
        response_add = requests.get(self.url+"/add",params=self.data)
        result = int(response_add.text[-1])
        self.assertEqual(result, 7)
    
    def test_sub(self):
        response_sub = requests.get(self.url+"/sub",params=self.data)
        result=int(response_sub.text[-1])
        self.assertEqual(result, 3)

if __name__ == "__main__":
    unittest.main()
